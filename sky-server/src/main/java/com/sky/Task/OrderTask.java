package com.sky.Task;

import com.sky.entity.Orders;
import com.sky.mapper.OrdersMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
@Slf4j
public class OrderTask {
    @Autowired
    private OrdersMapper ordersMapper;
    //每一分钟执行一次,支付超过十五分钟直接取消支付修改订单状态
//    @Scheduled(cron = "0/5 * * * * ?")
    public void updateStatusByNoPay() {
         log.info("未支付超时订单,取消订单{}", LocalDateTime.now());
         //查询超时订单大于createTime
        Orders orders = new Orders();
        orders.setStatus(Orders.PENDING_PAYMENT);
        orders.setOrderTime(LocalDateTime.now().plusMinutes(-15));
       List<Orders> list =  ordersMapper.selectByStatusAndCreateTimeLT(orders);
        for (Orders orders1 : list) {
            orders1.setStatus(Orders.CANCELLED);
            orders1.setCancelReason("太忙了");
            orders1.setCancelTime(LocalDateTime.now());
            ordersMapper.update(orders1);
        }
    }
    //打烊一小时过后将所有在派送订单已完成
//    @Scheduled(cron = "0/15 * * * * ?")
    public void updateStatusBy() {
       log.info("派送中订单打烊后一小时",LocalDateTime.now());
       //时间小于0.00
        //TODO 未实现功能
        Orders orders = new Orders();
        orders.setStatus(Orders.DELIVERY_IN_PROGRESS);
        orders.setOrderTime(LocalDateTime.now().plusMinutes(60));
        List<Orders>  list =  ordersMapper.selectByStatusAndCreateTimeLT(orders);
        for (Orders orders1 : list) {
            orders1.setStatus(orders1.COMPLETED);
            ordersMapper.update(orders1);
        }
    }
}
